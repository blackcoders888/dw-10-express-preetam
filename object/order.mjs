//in array, order of elements matter.
let ar1 = [1,2,3,4]
console.log(ar1[0])

//in object, order of elements does not matter.
let info={
  name:"ram",
  age:23,
  name:"hari"

}
console.log(info.name)
