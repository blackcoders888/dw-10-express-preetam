//interview questions

// this(operator)
//this operator is used to print the value inside the object.
//this operator le object bhitra ko details lai print garna help garxa just like this example.
//this operator are those which points itself.
//arrow function does not support this operator.

let info = {
  name: "preetam",
  surName: "pakuwal",
  age: 23,
  fullDetails: function () {
    console.log(`my name is ${this.name} ${this.surName} my age is${this.age}`);
  },
  info1: {
    info2: { address: "kadaghari" },
    favFood: ["apple", "banana"],
  },
};

console.log(info.name);
console.log(info.age);
info.fullDetails();
console.log(info.info1.info2);
console.log(info.info1.info2.address);
console.log(info.info1.favFood);

// . chai object lai matra milxa.