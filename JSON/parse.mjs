// console.log(JSON.parse("null")); // null
// console.log(JSON.parse("1")); // 1
// console.log(JSON.parse("[1, 2, 3]")); // [1, 2, 3]
// [1,2], [1, 3], [1,3] ======= [1, 2], [1, 3]
let a = [1, 2, [1, 3], [1, 3]];
let ar1 = a.map((value, i) => {
  return JSON.stringify(value);
});
let ar2 = [...new Set(ar1)];
let ar3 = ar2.map((value, i) => {
  return JSON.parse(value);
});
console.log(ar3);
// [{name:"nitan",age:29},{name:"ram",age:30},{name:"nitan",age:29}]  =====[{name:"nitan",age:29},{name:"ram",age:30}]
let info = [
  { name: "nitan", age: 29 },
  { name: "ram", age: 30 },
  { name: "nitan", age: 29 },
];
let arr1 = info.map((value, i) => {
  return JSON.stringify(value);
});
console.log(arr1);
let arr2 = [...new Set(arr1)];
console.log(arr2);
let arr3 = arr2.map((value, i) => {
  return JSON.parse(value);
});
console.log(arr3);
let i = [[1,2], [1, 3], [1,3]]
let j = [[1,2], ...[1, 3], ...[1,3]]
// let k = [[1, 2]]//
let k = [...new Set(j)]
let l = k.join(",")
console.log(l)
console.log(k)
