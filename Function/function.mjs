//function has two parts
//function definition
//function call
let sum = function () {
  console.log("sum");
};
console.log("a");
sum();
console.log("b");

//we can defined the variable and function like this also.
// function summ() {
//   console.log("aabb");
// }
// summ();
