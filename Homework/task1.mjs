//make a function called sum
//pass two value
//function must return the sum of two value

// let sum = (a, b) => {
//   let _sum = a + b;
//   console.log(_sum);
// };
// sum(1, 2);

export let sum = (a, b) => {
  let _sum = a + b;
  return _sum;
};
