//make a arrow function that takes a input and return its upperCase version.
export let upper = (a) => {
  return a.toUpperCase();
};


//make a arrow function that takes a input and return its lowerCase version.
export let lower=(b)=>{
let _lower= b.toLowerCase()
return _lower
}


//make a arrow function that takes a input and return its replaceAll version.
export let _replace=(str,by, to)=>{
  let __replace=str.replaceAll(by,to)
  return __replace
}

// make a arrow function that takes a input and return another string which is a trim version (remove both start and end space)
export let name=(d)=>{
  let _name=d.trimEnd()
  return _name
}


// make a arrow function that takes a input return true if the string starts with Bearear else return false