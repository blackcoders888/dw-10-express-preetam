//primitive
//numbers, boolean, string, undefined parxa

//non-primitive
//object, array, null, error, set parxa

//in primitive memory allocation is done whenever the let is created.
//if let is used, memory will allocate for each new let.

// let a=1
// let b=a
// a=5
// console.log(a)
// console.log(b)



//in non-primitive the memory allocation is done while checking whether variables shares same data or not.
//if there is let a new memory space is created but first it see whether the variable is copy of another variable.
//if it is shared then it used shared memory.
let ar1= [1,2]
let ar2=ar1
ar1.push(3)
console.log(ar1)
console.log(ar2)


//===
//primitive
//in case of primitive, it checks the value or data.
let a=1
let b=1
let c=a
console.log(a===b)
console.log(a===c)

//===
//non-primitive
//in case of non-primitive, it checks the memory location whether it falls under same location or not. if so, it is true else false.
let x=[1,2,3]
let y=x
let z=[1,2]
console.log(x===y)
console.log(z===x)


//in this [1,2] is data but it doesn't create the memory allocation so it gives false result. bhada baneko xaina. bhada=memory.
// console.log([1,2]===[1,2])

