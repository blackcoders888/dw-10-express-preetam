// set can only remove duplicate values of primitive data types
// since array is non-primitive it's duplicate values is not removed but null is primitive
let ar2 = [1, false, null, null, [1, 2], [1, 2]]
let uniqueValues = [...new Set(ar2)]
console.log(uniqueValues)
