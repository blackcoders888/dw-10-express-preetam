// set will remove the duplicate values.
// [1,2,1,3,4,4,5]

let s1 = new Set([1,2,1,3,4,4,5])
console.log(s1)  //output => Set(5) {1,2,3,4,5}    //here, Set(5) bhaneko length ho.


let s2 = new Set([1,2,1,3,4,4,5])
let s3 = [...s2]    //output => 1,2   //spread operator   // ... bhaneko spread operator.
console.log(s3)     //output => [1,2]