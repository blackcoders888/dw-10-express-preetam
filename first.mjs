// // console.log("my name is preetam pakuwal");
// // console.log("my name is gagal");
// // console.log("my name is preetam")
// // console.log("my name is pakuwal")

// let name = "preetam";
// let age = 23;
// let ismarried = false;

// console.log(name);
// console.log(age)
// console.log(ismarried);

//add, sub, mul, divide
// let a = 1;
// let b = 2;

// let c = a + b;
// let d = a - b;
// let e = a * b;
// let f = b / a;

// console.log(c, d, e, f);

//arithmetic operator = (+,-,*,/,%)
//% = remainder, modulo, percentile

console.log(3 === 1); //false  //equal to
console.log(3 == 3); //true

console.log(3 != 1); //true   //doesn't equal to
console.log(3 != 3); //false

console.log(1 > 2); //false
console.log(2 > 1); //true

console.log(1 >= 1); //true

//logical operator
console.log(true && false && true && false); //false
console.log(false && false && false && false); //false
console.log(true && false && true && false); //false
console.log(true && true && true); //true

console.log(true || false || true || false); //true
console.log(false || false || false || false); //false
console.log(true || false || true || false); //true
console.log(true || true || true); //true

//for select all and or or operator at once double click and select one and or or operator and then press ctrl + D one after another and backspace it.

//logical operator ! (makes true to false and false to true)
console.log(!true); //false

//assignment equal to (=, +=, -=, /=, %=)
let a = 1;
let b = 2;
b = b + a; //we can either write b+=a
console.log(b);

//using descriptive name
let num1 = 1;
let num2 = 2;
let sum;
sum = num1 + num2;
console.log(sum);

console.log(Number("2.45")); //converting the string into number using "Number" keyword. But not applicable for alphabet to number.
console.log(parseInt("2.45")); //only give integer value like 2.

//String
// String(1) -> "1"
// String("true") -> "true"
console.log(String(1)); //string comes in white color in terminal
console.log(String(true));

console.log(Number("1"));
