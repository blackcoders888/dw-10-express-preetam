let str="   preetam pakuwal    "
//length
let strLength = str.length //this will also count the spaces.
console.log(str.length)


//uppercase
// let str="   preetam pakuwal    "
let strUpper= str.toUpperCase()
console.log(strUpper)

//lowercase
// let str="   preetam pakuwal    "
let strLower=str.toLowerCase()
console.log(strLower)

//trim
// let str="   preetam pakuwal    "
let strTrim= str.trim()
console.log(strTrim)

//trimStart
// let str="   preetam pakuwal    "
let strTrimStart = str.trimStart()
console.log(strTrimStart)

//trimEnd
// let str="   preetam pakuwal    "
let strTrimEnd=str.trimEnd()
console.log(strTrimEnd)

//includes
// let str="   preetam pakuwal    "
let haspreetam = str.includes("pre")  //checks whether the certain string is in the input or not. Also lowercase xa bhane lower mai count garxa, upper lidaina and vice versa.
console.log(haspreetam)

//replaceAll
let name= "preetam pakuwal is my name"
let newName= name.replaceAll("is","was")
console.log(newName)

//replaceAll
let _name= "preetam pakuwal"
let _newName= name.replaceAll("p","b")
console.log(_newName)

//replace
let __name= "preetam pakuwal"
let __newName= name.replace(" ",",")
console.log(__newName)