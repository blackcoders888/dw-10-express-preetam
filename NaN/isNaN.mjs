// isNaN function check if the passed argument is not a number or not 
// --- is not a number 
// if the string can be converted into number then isNaN is false 
// console.log(isNaN(1))
// console.log(isNaN("1")) // since "1" can be converted into number 
// console.log(isNaN(false))
console.log(isNaN(null))
let a = Number("1")
console.log(typeof(a))
