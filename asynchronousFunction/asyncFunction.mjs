// console.log("a");
// setTimeout(() => {
//   console.log("i am setTimeout");
// }, 3000);
// console.log("b");
//output
//a
//b
//i am setTimeout





console.log("a");
setTimeout(() => {
  console.log("i am setTimeout1");
}, 3000);

setTimeout(() => {
  console.log("i am setTimeout2");
}, 0);
console.log("b");
//output
//a
//b
//i am setTimeout2
//i am setTimeout1






//js have call stack while node has memory queue.

//setTimeout = in js there is call stack(call stack run only one line of code and pops out after it runs), call stack sends the setTimeout code into node attached with timer. After that timer it sends the code into the memory queue of node. In memory queue it checks in call stack whether the call stack is empty or not. if empty then memory queue sends back to call stack for execution.

//anything that push the task into background is called asynchronous function.

//in case of backend, node is the background whereas in case of frontend browser is the background.

//call stack are those which runs javascript code.


//memory queue ma jun function pahela janxa tehi nai pahela call stack ma gayera execute hunxa.
//if memory queue full bhayo ra function node ma xa ra call stack pani free xaina bhane program nai crash hunxa.
//code jati synchronous code ho except settimeout and setinterval (these are called asynchronous code). asynchronous code le background ma code falxa which is called asynchronous task. and synchronous code is called synchronous task.
//synchronous task sabbai complete bhaye paxi matrai asynchronous task complete hunxa.


//event loop = it constantly monitor call stack and memory queue.
//event loop = if call stack is empty it will move function in memory queue to call stack.
